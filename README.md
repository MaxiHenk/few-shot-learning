# Goal of this repository 
This repository is an easy to use Few Shot Learning Tool. It combines two other libraries i.e. Easy Few Shot Learning (https://github.com/sicara/easy-few-shot-learning) and learn2learn (https://github.com/learnables/learn2learn/tree/master/learn2learn). Right now it supports MAML, Meta-SGD, Protoypical networks and Matching Networks. It allows easy wrapping of .jpg files for the Networks and creates visualization of a learning progress. The repository was tested on Files from MiniImageNet, CUB, Omniglot and a costum FESTO-Dataset, that shows how well Few Shot Learning works with an Industrial Dataset. The wiki contains a description, how it works.
<br>
<br> 
For viewing the results and getting an insight into Few Shot Learning, you can additionally read the thesis in this repository.
