#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import sys
import pathlib
import os
import random
import math

MAIN_DIR = pathlib.Path(__file__).parent.parent.parent.absolute()
sys.path.append(MAIN_DIR)

def create_JSON_file(dataset, image_folder,trainsize, testsize, valsize):
    data_dir = str(MAIN_DIR) + '/data/' + dataset
    image_dir = data_dir + '/' + image_folder
    
    #getting all the folders, which are used for training
    folders = os.listdir(image_dir)
    
    #splitting them
    random.shuffle(folders)
    train_arr, test_arr, val_arr = split_array_into_three(folders, trainsize, testsize, valsize)
    
    #creating the class roots for the pictures:
    train_class_roots = create_class_roots(train_arr, image_dir)
    test_class_roots = create_class_roots(test_arr, image_dir)
    val_class_roots = create_class_roots(val_arr, image_dir)
    
    #writes the file into json files:
    write_to_files(train_arr, train_class_roots, "train", data_dir)
    write_to_files(test_arr, test_class_roots, "test", data_dir)
    write_to_files(val_arr, val_class_roots, "val", data_dir)
    
    
def split_array_into_three(arr, train, test, val):
    if (test + train + val) != 1.0 or test < 0 or train < 0 or val < 0:
        raise Exception("Percentages need to add up to 1 and need to be positive")
        
    #one number can be rounded up the others need to be rounded down, so no dataset is used twice
    len_train = math.ceil(len(arr) * train)
    len_test = math.trunc(len(arr) * test)
    len_val = math.trunc(len(arr) * val)
    
    #puts them in the different arrays
    train_arr, test_arr, val_arr = [], [], []
    for i in range(len(arr)):
        if i < len_train:
            train_arr.append(arr[i])
        elif i <= (len_train + len_test) and i > len_train:
            test_arr.append(arr[i])
        else:
            val_arr.append(arr[i])
    return train_arr, test_arr, val_arr 

def create_class_roots(arr, path):
    class_roots = []
    for i in range(len(arr)):
        class_root = os.path.join(path, arr[i])
        class_roots.append(class_root)
    return class_roots

def write_to_files(imgs, class_roots, dataset, data_dir):
    with open(data_dir + "/" + dataset + ".json", "w") as f:
        data = {
            "class_names": imgs,
            "class_roots": class_roots
            } 
        json.dump(data, f, indent = 4)
    

