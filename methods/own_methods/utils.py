import numpy as np
import torch


def accuracy(predictions, targets):
    predictions = predictions.argmax(dim=1).view(targets.shape)
    return (predictions == targets).sum().float() / targets.size(0)

def fast_adapt(batch, learner, loss, adaptation_steps, shots, ways, device):
    data, labels = batch
    data, labels = data.to(device), labels.to(device)

    # Separate data into adaptation/evalutation sets
    adaptation_indices = np.zeros(data.size(0), dtype=bool)
    adaptation_indices[np.arange(shots*ways) * 2] = True
    evaluation_indices = torch.from_numpy(~adaptation_indices)
    adaptation_indices = torch.from_numpy(adaptation_indices)
    adaptation_data, adaptation_labels = data[adaptation_indices], labels[adaptation_indices]
    evaluation_data, evaluation_labels = data[evaluation_indices], labels[evaluation_indices]
    
    # Adapt the model
    for step in range(adaptation_steps):
        train_error = loss(learner(adaptation_data), adaptation_labels)
        learner.adapt(train_error)

    # Evaluate the adapted model
    predictions = learner(evaluation_data)
    valid_error = loss(predictions, evaluation_labels)
    valid_accuracy = accuracy(predictions, evaluation_labels)
    return valid_error, valid_accuracy

def evaluate(loss, shots, model, taskset, meta_batch_size, adaption_steps, n_way, device, seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    
    meta_test_error = 0.0
    meta_test_accuracy = 0.0
    for task in range(meta_batch_size):
        # Compute meta-testing loss
        learner = model.clone()
        batch = taskset.sample()
        evaluation_error, evaluation_accuracy = fast_adapt(batch,
                                                            learner,
                                                            loss,
                                                            adaption_steps,
                                                            shots,
                                                            n_way,
                                                            device)
        meta_test_error += evaluation_error.item()
        meta_test_accuracy += evaluation_accuracy.item()
    print('Meta Test Error', meta_test_error / meta_batch_size)
    print('Meta Test Accuracy', meta_test_accuracy / meta_batch_size)
    return meta_test_accuracy / meta_batch_size

def train(train_loader, loss, shots, model, opt, writer, val_loader, val_freq, 
          training_episodes, meta_batch_size, adaption_steps, n_way, device, seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    
    for iteration in range(training_episodes):
        opt.zero_grad()
        meta_train_error = 0.0
        meta_train_accuracy = 0.0
        meta_valid_error = 0.0
        meta_valid_accuracy = 0.0
        for task in range(meta_batch_size):
            # Compute meta-training loss
            learner = model.clone()
            batch = train_loader.sample()
            evaluation_error, evaluation_accuracy = fast_adapt(batch,
                                                               learner,
                                                               loss,
                                                               adaption_steps,
                                                               shots,
                                                               n_way,
                                                               device)
            evaluation_error.backward()
            meta_train_error += evaluation_error.item()
            meta_train_accuracy += evaluation_accuracy.item()
            
            if val_loader != None:
                # Compute meta-validation loss
                learner = model.clone()
                batch = val_loader.sample()
                evaluation_error, evaluation_accuracy = fast_adapt(batch,
                                                                   learner,
                                                                   loss,
                                                                   adaption_steps,
                                                                   shots,
                                                                   n_way,
                                                                   device)
                meta_valid_error += evaluation_error.item()
                meta_valid_accuracy += evaluation_accuracy.item()
        
        #save some metrics
        print('Iteration:', iteration)
        print('Train_Loss', (meta_train_error / meta_batch_size))
        print('Train_acc', (meta_train_accuracy / meta_batch_size))
        writer.add_scalar('Train_acc', (meta_train_accuracy / meta_batch_size), meta_batch_size * iteration)
        writer.add_scalar('Train_Loss', (meta_train_error / meta_batch_size), meta_batch_size * iteration)
        
        if val_loader != None:
            writer.add_scalar('Val_Loss', (meta_train_error / meta_batch_size), meta_batch_size * iteration)
            writer.add_scalar('Val_acc', (meta_valid_accuracy / meta_batch_size), meta_batch_size * iteration)
            print('Val_Loss', (meta_train_error / meta_batch_size))
            print('Val_acc', (meta_valid_accuracy / meta_batch_size))
        print('\n')
            

        # Average the accumulated gradients and optimize
        for p in model.parameters():
            p.grad.data.mul_(1.0 / meta_batch_size)
        opt.step()