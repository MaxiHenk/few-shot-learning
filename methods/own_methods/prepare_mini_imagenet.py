"""
Run this script to prepare the miniImageNet dataset.

This script uses the 100 classes of 600 images each used in the Matching Networks paper. The exact images used are
given in data/mini_imagenet.txt which is downloaded from the link provided in the paper (https://goo.gl/e3orz6).

1. Download files from 
https://drive.google.com/file/d/0B3Irx3uQNoBMQ1FlNXJsZUdYWEE/view
 and place in
    data/mini-imagenet/images
2. Run the script
"""
from tqdm import tqdm as tqdm
import numpy as np
import shutil
import os

def mkdir(dir):
    """Create a directory, ignoring exceptions

    # Arguments:
	dir: Path of directory to create
    """
    try:
        os.mkdir(dir)
    except:
        pass


def rmdir(dir):
    """Recursively remove a directory and contents, ignoring exceptions

   # Arguments:
   dir: Path of directory to recursively remove
   """
    try:
        shutil.rmtree(dir)
    except:
        pass
    
def create_image_folder(datapath, image_file_name):

    mkdir(datapath + '/mini-imagenet/images')

    # Find class identities
    classes = []
    for root, _, files in os.walk(datapath + '/mini-imagenet/' + image_file_name):
        for f in files:
            if f.endswith('.jpg'):
                classes.append(f[:-12])
    
    classes = list(set(classes))


    # Create class folders
    for c in classes:
        mkdir(datapath + f'/mini-imagenet/images/{c}/')

    
    # Move images to correct location
    for root, _, files in os.walk(datapath + '/mini-imagenet/' + image_file_name):
        for f in tqdm(files, total=600*100):
            if f.endswith('.jpg'):
                class_name = f[:-12]
                image_name = f[-12:]
                src = f'{root}/{f}'
                dst = datapath + f'/mini-imagenet/images/{class_name}/{image_name}'
                shutil.copy(src, dst)
