import os
from pathlib import Path
import re
from shutil import copyfile

PATH = Path(os.getcwd())

HOME_PATH = PATH.parent.parent.absolute()

DATA_PATH = str(HOME_PATH) + '/data'
PATH_FESTO = DATA_PATH + '/Festo/images'
PATH_FESTO_SEL = DATA_PATH + '/Festo_selected'

if not(os.path.exists(PATH_FESTO_SEL)):
    os.mkdir(PATH_FESTO_SEL)
    
for root, dirs, files in os.walk(PATH_FESTO):
    for names in dirs:
        CURR_FILE = os.path.join(PATH_FESTO, names)
        CURR_FILE_COPY = os.path.join(PATH_FESTO_SEL, names)
        if not(os.path.exists(CURR_FILE_COPY)):
               os.mkdir(CURR_FILE_COPY)
        for root2, dirs2, files2 in os.walk(CURR_FILE):
            for name in files2:
                image_number_jpg = re.findall('[0-9]*.jpg', name)
                image_number = re.findall('[0-9]*', image_number_jpg[0])
                image_number = int(image_number[0])
                if (image_number % 28 in range(7,21)):
                    copyfile(CURR_FILE + '/' + name, CURR_FILE_COPY + '/' + name)
                    
                